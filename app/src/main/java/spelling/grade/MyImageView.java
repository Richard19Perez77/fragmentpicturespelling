package spelling.grade;

import android.os.Handler;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by Rick on 2/27/2016.
 */
public class MyImageView {

    ImageData imageData = ImageData.getInstance();

    ImageView wordImageView;

    ImageFragment.OnFragmentInteractionListener mListener;

    public MyImageView(ImageFragment.OnFragmentInteractionListener l) {
        mListener = l;
    }

    public void setWordImageView(View view) {
        //get reference to the imageview
        wordImageView = (ImageView) view.findViewById(R.id.wordImageView);

        wordImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                String correctString = "";

                //get correct answer string
                switch (imageData.currentImageGrade) {
                    case 1:
                        correctString = imageData.firstWords[imageData.currentImageNumber];
                        break;
                    case 2:
                        correctString = imageData.secondWords[imageData.currentImageNumber];
                        break;
                    case 3:
                        correctString = imageData.thirdWords[imageData.currentImageNumber];
                        break;
                    case 4:
                        correctString = imageData.fourthWords[imageData.currentImageNumber];
                        break;
                    case 5:
                        correctString = imageData.fifthWords[imageData.currentImageNumber];
                        break;
                    default:
                        break;
                }

                int answerCharCount = correctString.length();
                String hint = "";

                if (answerCharCount == 0) {
                    hint = "Press a grade button!";
                }else{
                    while (answerCharCount > 0) {
                        hint = hint + " * ";
                        answerCharCount--;
                    }
                    hint = "HINT: " + hint;
                }

                mListener.showToast(hint);

                return false;
            }
        });
    }

    public void setNewImage(int grade, final int picture) {
        switch (grade) {
            case 1:

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        wordImageView.setBackgroundResource(imageData.firstImageList[picture]);
                    }
                });

                break;
            case 2:

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        wordImageView.setBackgroundResource(imageData.secondImageList[picture]);
                    }
                });

                break;
            case 3:

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        wordImageView.setBackgroundResource(imageData.thirdImageList[picture]);
                    }
                });

                break;
            case 4:

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        wordImageView.setBackgroundResource(imageData.fourthImageList[picture]);
                    }
                });

                break;
            case 5:

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        wordImageView.setBackgroundResource(imageData.fifthImageList[picture]);
                    }
                });

                break;
            default:
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        wordImageView.setBackgroundResource(R.drawable.intro);
                    }
                });
                break;
        }
    }
}
