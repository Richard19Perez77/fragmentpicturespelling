package spelling.grade;

import android.graphics.Bitmap;

public class Star {
    Bitmap starbitmap;
    int x, y;
    int speed;
    int direction;

    public Star() {
        x = 0;
        y = 0;
        speed = 0;
        direction = 0;
    }
}
