package spelling.grade;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.SparseIntArray;

/**
 * Created by Rick on 2/26/2016.
 */
public class MySoundPool {

    int check = 1;
    int x = 2;
    int win = 3;

    float fSpeed = 1f;

    SparseIntArray soundsMap;

    SoundPool soundPool;

    public MySoundPool(Context context){
        soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 100);

        soundsMap = new SparseIntArray();
        soundsMap.put(check, soundPool.load(context, R.raw.check, 1));
        soundsMap.put(x, soundPool.load(context, R.raw.x, 1));
        soundsMap.put(win, soundPool.load(context, R.raw.win, 1));
    }

    public void playSound(int sound, float volume){
        soundPool.play(soundsMap.get(sound), volume, volume, 1, 0, fSpeed);
    }
}