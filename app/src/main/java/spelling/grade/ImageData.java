package spelling.grade;

import java.util.ArrayList;

/**
 * Created by Richard A. Perez on 2/26/2016.
 */
public class ImageData {

    private ImageData(){

    }

    public static ImageData getInstance(){
        if(instance == null){
            instance = new ImageData();
        }
        return instance;
    }

    private static ImageData instance = null;

    public int currentImageGrade = -1;

    public int currentImageNumber = -1;

    public float currentRating = 0;

    public String currentSpellingText = "";

    public boolean isCheckShown = true;

    String[] firstWords = { "star", "bus", "cake", "boat", "barn", "cat",
            "dog", "pan", "pig", "apple", "banana", "bone", "pin", "mop",
            "one", "two", "three", "four", "five", "six", "seven", "eight",
            "nine", "ten", "bat", "bed", "hat", "snake" };

    String[] secondWords = { "sword", "tree", "cow", "shark", "bird",
            "foot", "horse", "sheep", "spider", "eleven", "twelve", "thirteen",
            "fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
            "nineteen", "twenty", "bear", "owl" };

    String[] thirdWords = { "ipod", "baseball", "guitar", "chair", "fox",
            "zipper", "airplane", "cherry", "hand", "mummy", "turkey", "shell",
            "tooth", "cube", "crayon", "earth", "table", "squirrel", "camel",
            "panda", "carrot" };

    String[] fourthWords = { "sunglasses", "watch", "blender", "heart",
            "lemon", "door", "cannon", "anvil", "penguin", "mountain", "piano",
            "helicopter", "motorcycle", "satellite", "snail", "basketball",
            "cactus", "flower", "ghost", "dolphin", "buffalo", "meteor" };

    String[] fifthWords = { "pumpkin", "chainsaw", "iceskate", "r2d2",
            "microwave", "elephant", "reindeer", "bomb", "penny", "wolf",
            "iceberg", "helmet", "umbrella", "propellar", "slingshot",
            "volcano", "mittens", "calculator", "tornado", "camera",
            "thousand", "cheetah" };

    Integer[] firstImageList = { R.drawable.star, R.drawable.bus,
            R.drawable.cake, R.drawable.boat, R.drawable.barn, R.drawable.cat,
            R.drawable.dog, R.drawable.pan, R.drawable.pig, R.drawable.apple,
            R.drawable.banana, R.drawable.bone, R.drawable.pin, R.drawable.mop,
            R.drawable.one, R.drawable.two, R.drawable.three, R.drawable.four,
            R.drawable.five, R.drawable.six, R.drawable.seven,
            R.drawable.eight, R.drawable.nine, R.drawable.ten, R.drawable.bat,
            R.drawable.bed, R.drawable.hat, R.drawable.snake };

    Integer[] secondImageList = { R.drawable.sword, R.drawable.tree,
            R.drawable.cow, R.drawable.shark, R.drawable.bird, R.drawable.foot,
            R.drawable.horse, R.drawable.sheep, R.drawable.spider,
            R.drawable.eleven, R.drawable.twelve, R.drawable.thirteen,
            R.drawable.fourteen, R.drawable.fifteen, R.drawable.sixteen,
            R.drawable.seventeen, R.drawable.eighteen, R.drawable.nineteen,
            R.drawable.twenty, R.drawable.bear, R.drawable.owl };

    Integer[] thirdImageList = { R.drawable.ipod, R.drawable.baseball,
            R.drawable.guitar, R.drawable.chair, R.drawable.fox,
            R.drawable.zipper, R.drawable.airplane, R.drawable.cherry,
            R.drawable.hand, R.drawable.mummy, R.drawable.turkey,
            R.drawable.shell, R.drawable.tooth, R.drawable.cube,
            R.drawable.crayon, R.drawable.earth, R.drawable.table,
            R.drawable.squirrel, R.drawable.camel, R.drawable.panda,
            R.drawable.carrot };

    Integer[] fourthImageList = { R.drawable.sunglasses, R.drawable.watch,
            R.drawable.blender, R.drawable.heart, R.drawable.lemon,
            R.drawable.door, R.drawable.cannon, R.drawable.anvil,
            R.drawable.penguin, R.drawable.mountain, R.drawable.piano,
            R.drawable.helicopter, R.drawable.motorcycle, R.drawable.satellite,
            R.drawable.snail, R.drawable.basketball, R.drawable.cactus,
            R.drawable.flower, R.drawable.ghost, R.drawable.dolphin,
            R.drawable.buffalo, R.drawable.meteor };

    Integer[] fifthImageList = { R.drawable.pumpkin, R.drawable.chainsaw,
            R.drawable.iceskate, R.drawable.r2d2, R.drawable.microwave,
            R.drawable.elephant, R.drawable.reindeer, R.drawable.bomb,
            R.drawable.penny, R.drawable.wolf, R.drawable.iceberg,
            R.drawable.helmet, R.drawable.umbrella, R.drawable.propellar,
            R.drawable.slingshot, R.drawable.volcano, R.drawable.mittens,
            R.drawable.calculator, R.drawable.tornado, R.drawable.camera,
            R.drawable.thousand, R.drawable.cheetah };

    ArrayList<Integer> firstWordsUsed = new ArrayList<>();

    ArrayList<Integer> secondWordsUsed = new ArrayList<>();

    ArrayList<Integer> thirdWordsUsed = new ArrayList<>();

    ArrayList<Integer> fourthWordsUsed = new ArrayList<>();

    ArrayList<Integer> fifthWordsUsed = new ArrayList<>();

}