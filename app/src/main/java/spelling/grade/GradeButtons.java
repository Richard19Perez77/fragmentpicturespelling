package spelling.grade;


import android.os.Handler;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by Rick on 2/26/2016.
 */
public class GradeButtons {

    final int FIRST = 1;
    final int SECOND = 2;
    final int THIRD = 3;
    final int FOURTH = 4;
    final int FIFTH = 5;

    ImageData imageData = ImageData.getInstance();

    Button first;
    Button second;
    Button third;
    Button fourth;
    Button fifth;

    Button checkButton;

    TextView spellingTextView;

    RatingBar ratingBar;

    ImageView answerImageView;

    SpellCheckUIFragment.OnFragmentInteractionListener mListener;


    public GradeButtons(SpellCheckUIFragment.OnFragmentInteractionListener listener) {
        mListener = listener;
    }

    public void hideSoftKeyboard() {
        mListener.hideSoftKeyboard();
    }

    public void setButtons(View view) {
        setFirstButton((Button) view.findViewById(R.id.firstButton));
        setSecondButton((Button) view.findViewById(R.id.secondButton));
        setThirdButton((Button) view.findViewById(R.id.thirdButton));
        setFourthButton((Button) view.findViewById(R.id.fourthButton));
        setFifthButton((Button) view.findViewById(R.id.fifthButton));

        setCheckButton((Button) view.findViewById(R.id.checkAnswer));

        setSpellingTextView((TextView) view.findViewById(R.id.answerText));

        setRatingBar((RatingBar) view.findViewById(R.id.gradeRatingBar));

        setAnswerImageView((ImageView) view.findViewById(R.id.answerImageView));
    }

    public void setAnswerImageView(ImageView iv) {
        answerImageView = iv;
    }

    public void setRatingBar(RatingBar rb) {
        ratingBar = rb;
    }

    public void setSpellingTextView(TextView t) {
        spellingTextView = t;
        spellingTextView.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
    }

    public void setCheckButton(Button b) {
        checkButton = b;
        checkButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                //get text from text if possible
                if (spellingTextView.getText() != null && spellingTextView.getText().length() > 0) {

                    final String answerString = spellingTextView.getText().toString().toLowerCase().replace(" ", "");
                    String correctString = "";

                    //get correct answer string
                    switch (imageData.currentImageGrade) {
                        case 1:
                            correctString = imageData.firstWords[imageData.currentImageNumber];
                            break;
                        case 2:
                            correctString = imageData.secondWords[imageData.currentImageNumber];
                            break;
                        case 3:
                            correctString = imageData.thirdWords[imageData.currentImageNumber];
                            break;
                        case 4:
                            correctString = imageData.fourthWords[imageData.currentImageNumber];
                            break;
                        case 5:
                            correctString = imageData.fifthWords[imageData.currentImageNumber];
                            break;
                    }

                    if (answerString.equals(correctString)) {
                        //add value to the rating bar
                        imageData.currentRating++;
                        final float stars = imageData.currentRating;

                        if (stars > 5) {

                            //reset game state
                            imageData.currentRating = 0;
                            imageData.currentImageNumber = -1;
                            imageData.currentImageGrade = -1;
                            spellingTextView.setText("");
                            ratingBar.setRating(0);
                            mListener.startWinActivity();

                        } else {

                            new Handler().post(new Runnable() {
                                @Override
                                public void run() {
                                    ratingBar.setRating(stars);
                                    answerImageView.setBackgroundResource(R.drawable.check);
                                    imageData.isCheckShown = true;
                                }
                            });

                            mListener.playCheckSound();

                        }

                    } else {

                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                imageData.currentRating = 0;
                                ratingBar.setRating(0);
                                answerImageView.setBackgroundResource(R.drawable.x);
                                imageData.isCheckShown = false;
                            }
                        });

                        mListener.playXSound();

                    }
                }
            }
        });
    }

    public void setFirstButton(Button b) {
        first = b;
        first.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                //get new number for image
                int temp = (int) Math.floor(Math.random() * imageData.firstImageList.length);

                //check if image has been selected before looping through list of images
                while (imageData.firstWordsUsed.contains(temp)) {
                    temp = (int) Math.floor(Math.random() * imageData.firstImageList.length);
                }

                //update stored image number
                imageData.currentImageNumber = temp;

                //update stored image grade
                imageData.currentImageGrade = FIRST;

                //add new number to the list
                imageData.firstWordsUsed.add(temp);

                //check if this made the list full, if so empty it
                if (imageData.firstWordsUsed.size() == imageData.firstImageList.length) {
                    imageData.firstWordsUsed.clear();
                }

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        spellingTextView.setText("");
                    }
                });

                //set new image to the image view
                mListener.onGradeButtonClick(FIRST, temp);
            }
        });
    }

    public void setSecondButton(Button b) {
        second = b;
        second.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                //get new number for image
                int temp = (int) Math.floor(Math.random() * imageData.secondImageList.length);

                //check if image has been selected before looping through list of images
                while (imageData.secondWordsUsed.contains(temp)) {
                    temp = (int) Math.floor(Math.random() * imageData.secondImageList.length);
                }

                //update stored image number
                imageData.currentImageNumber = temp;

                //update stored image grade
                imageData.currentImageGrade = SECOND;

                //add new number to the list
                imageData.secondWordsUsed.add(temp);

                //check if this made the list full, if so empty it
                if (imageData.secondWordsUsed.size() == imageData.secondImageList.length) {
                    imageData.secondWordsUsed.clear();
                }

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        spellingTextView.setText("");
                    }
                });

                //set new image to the image view
                mListener.onGradeButtonClick(SECOND, temp);
            }
        });
    }

    public void setThirdButton(Button b) {
        third = b;
        third.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                //get new number for image
                int temp = (int) Math.floor(Math.random() * imageData.thirdImageList.length);

                //check if image has been selected before looping through list of images
                while (imageData.thirdWordsUsed.contains(temp)) {
                    temp = (int) Math.floor(Math.random() * imageData.thirdImageList.length);
                }

                //update stored image number
                imageData.currentImageNumber = temp;

                //update stored image grade
                imageData.currentImageGrade = THIRD;

                //add new number to the list
                imageData.thirdWordsUsed.add(temp);

                //check if this made the list full, if so empty it
                if (imageData.thirdWordsUsed.size() == imageData.thirdImageList.length) {
                    imageData.thirdWordsUsed.clear();
                }

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        spellingTextView.setText("");
                    }
                });

                //set new image to the image view
                mListener.onGradeButtonClick(THIRD, temp);
            }
        });
    }

    public void setFourthButton(Button b) {
        fourth = b;
        fourth.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                //get new number for image
                int temp = (int) Math.floor(Math.random() * imageData.fourthImageList.length);

                //check if image has been selected before looping through list of images
                while (imageData.fourthWordsUsed.contains(temp)) {
                    temp = (int) Math.floor(Math.random() * imageData.fourthImageList.length);
                }

                //update stored image number
                imageData.currentImageNumber = temp;

                //update stored image grade
                imageData.currentImageGrade = FOURTH;

                //add new number to the list
                imageData.fourthWordsUsed.add(temp);

                //check if this made the list full, if so empty it
                if (imageData.fourthWordsUsed.size() == imageData.fourthImageList.length) {
                    imageData.fourthWordsUsed.clear();
                }

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        spellingTextView.setText("");
                    }
                });

                //set new image to the image view
                mListener.onGradeButtonClick(FOURTH, temp);
            }
        });
    }

    public void setFifthButton(Button b) {
        fifth = b;
        fifth.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                //get new number for image
                int temp = (int) Math.floor(Math.random() * imageData.fifthImageList.length);

                //check if image has been selected before looping through list of images
                while (imageData.fifthWordsUsed.contains(temp)) {
                    temp = (int) Math.floor(Math.random() * imageData.fifthImageList.length);
                }

                //update stored image number
                imageData.currentImageNumber = temp;

                //update stored image grade
                imageData.currentImageGrade = FIFTH;

                //add new number to the list
                imageData.fifthWordsUsed.add(temp);

                //check if this made the list full, if so empty it
                if (imageData.fifthWordsUsed.size() == imageData.fifthImageList.length) {
                    imageData.fifthWordsUsed.clear();
                }

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        spellingTextView.setText("");
                    }
                });

                //set new image to the image view
                mListener.onGradeButtonClick(FIFTH, temp);
            }
        });
    }

    public void init() {
        //resume state of objects
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                spellingTextView.setText(imageData.currentSpellingText);
                ratingBar.setRating(imageData.currentRating);
                if (imageData.isCheckShown)
                    answerImageView.setBackgroundResource(R.drawable.check);
                else
                    answerImageView.setBackgroundResource(R.drawable.x);
            }
        });
    }
}