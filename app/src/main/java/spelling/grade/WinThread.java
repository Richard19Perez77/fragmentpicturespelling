package spelling.grade;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class WinThread extends Thread {

    SurfaceHolder surfaceHolder;
    WinPanel panel;
    public boolean run = false;

    public WinThread(SurfaceHolder sh, WinPanel boardPanel) {
        surfaceHolder = sh;
        panel = boardPanel;
    }

    public void setRunning(boolean r) {
        run = r;
    }

    public SurfaceHolder getSurfaceHolder() {
        return surfaceHolder;
    }

    @Override
    public void run() {
        Canvas c;
        while (run) {
            c = null;
            try {
                c = surfaceHolder.lockCanvas(null);
                synchronized (surfaceHolder) {
                    if (c != null)
                        panel.draw(c);
                }
            } finally {
                // if an exception is thrown during the above
                // we don't leave the surface in an inconsistent state
                if (c != null) {
                    surfaceHolder.unlockCanvasAndPost(c);
                }
            }
        }
    }
}
