package spelling.grade;

import java.io.IOException;
import java.util.Random;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

@SuppressLint("ViewConstructor")
public class WinPanel extends SurfaceView implements SurfaceHolder.Callback {

    char[] oldColor;
    WinThread winThread;
    boolean retry, loadingPrepared = false,

    stepNextColorDigit = false, moveLetters = false;
    int screenH, screenW, acc = 0, textSize1, i, starCount, cycle = 0,
            downY = 0, downX = 0, upX = 0, upY = 0, diffX = 0, diffY = 0, aW,
            aH, points, maxPoints, currMp = 0;
    String currColor = "#0000FF", thanksString;
    float thanksX;
    Paint thanksPaint, spellingPaint, spellingPaintShadow;
    Star[] stars;
    Bitmap bitmapA;
    Random random;
    Point[] aArray;
    Point point;

    public WinPanel(Context context) {
        super(context);
        getHolder().addCallback(this);
        winThread = new WinThread(getHolder(), this);
        setFocusable(true);
    }

    public boolean performClick() {
        super.performClick();
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        synchronized (winThread.getSurfaceHolder()) {
            performClick();
            // initialize new point array
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                downX = (int) event.getX();
                downY = (int) event.getY();
                aArray = new Point[maxPoints];
                points = 0;
                moveLetters = false;
            }

            if (points < maxPoints) {
                point = new Point((int) event.getX(), (int) event.getY());
                aArray[points] = point;
                points++;
            }

            if (event.getAction() == MotionEvent.ACTION_UP) {
                upX = (int) event.getX();
                upY = (int) event.getY();

                diffX = upX - downX;
                diffY = upY - downY;

                moveLetters = true;
            }

        }
        return true;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        drawBackground(canvas);
    }

    private void drawBackground(Canvas canvas) {
        acc++;
        if (!loadingPrepared) {
            initVars();
            loadingPrepared = true;
        }

        // draw background first
        canvas.drawColor(Color.parseColor(currColor));

        drawLetters(canvas);
        moveAndDrawStars(canvas);

        thanksString = "Thank you For Playing";
        thanksX = thanksPaint.measureText(thanksString);
        canvas.drawText(thanksString, (screenW - thanksX) / 2, screenH / 4,
                thanksPaint);

        rotateColor();

        thanksString = "Picture Spelling Grade!";
        thanksX = spellingPaint.measureText(thanksString);
        canvas.drawText(thanksString, (screenW - thanksX) / 2, screenH / 2,
                spellingPaintShadow);

        thanksString = "Picture Spelling Grade!";
        thanksX = spellingPaint.measureText(thanksString);
        canvas.drawText(thanksString, (screenW - thanksX) / 2 + 2,
                screenH / 2 + 2, spellingPaint);

        thanksString = "Radical\u2605Appwards";
        thanksX = thanksPaint.measureText(thanksString);
        canvas.drawText(thanksString, (screenW - thanksX) / 2, screenH / 2
                + screenH / 4, thanksPaint);

        incrementCurrColor();
    }

    public void drawLetters(Canvas canvas) {
        for (int i = 0; i < points; i++) {
            canvas.drawBitmap(bitmapA, aArray[i].x, aArray[i].y, null);
        }
        if (moveLetters) {
            for (int i = 0; i < points; i++) {
                switch (i % 8) {
                    case 0:
                        aArray[i].x--;
                        break;
                    case 1:
                        aArray[i].x++;
                        aArray[i].y++;
                        break;
                    case 2:
                        aArray[i].x++;
                        aArray[i].y--;
                        break;
                    case 3:
                        aArray[i].x--;
                        aArray[i].y++;
                        break;
                    case 4:
                        aArray[i].x--;
                        aArray[i].y--;
                        break;
                    case 5:
                        aArray[i].y++;
                        break;
                    case 6:
                        aArray[i].y--;
                        break;
                    case 7:
                        aArray[i].x--;
                        break;
                }
            }
        }
    }

    public void rotateColor() {
        if (spellingPaint.getColor() == Color.BLUE)
            spellingPaint.setColor(Color.RED);
        else if (spellingPaint.getColor() == Color.RED)
            spellingPaint.setColor(Color.YELLOW);
        else if (spellingPaint.getColor() == Color.YELLOW)
            spellingPaint.setColor(Color.BLUE);
        else
            spellingPaint.setColor(Color.BLUE);
    }

    public void moveAndDrawStars(Canvas canvas) {
        for (int i = 0; i < starCount; i++) {
            stars[i].y -= stars[i].speed + 1;
            stars[i].x += 1 * stars[i].direction;

            // bounds checking top
            if (stars[i].y < 0 - stars[i].starbitmap.getHeight()) {
                stars[i].y = screenH;
                stars[i].x = random.nextInt(screenW);
                stars[i].speed = random.nextInt(5);
                // checking left or right
            } else if (stars[i].x < 0 - stars[i].starbitmap.getWidth()
                    || stars[i].x > screenW + stars[i].starbitmap.getHeight()) {
                stars[i].y = screenH;
                stars[i].x = random.nextInt(screenW);
                stars[i].speed = random.nextInt(5);
            }

            canvas.drawBitmap(stars[i].starbitmap, stars[i].x, stars[i].y, null);
        }
    }

    public void initVars() {
        // landscape switches these two... i think... lets see : )
        screenH = getHeight();
        screenW = getWidth();

        setScreenVarsSizes();

        stars = new Star[starCount];
        random = new Random();

        for (int i = 0; i < starCount; i++) {
            stars[i] = new Star();
            if (i < starCount / 2) {
                stars[i].starbitmap = BitmapFactory.decodeResource(
                        getResources(), R.drawable.star1);
                stars[i].y = screenH;
                stars[i].x = random.nextInt(screenW);
                stars[i].speed = random.nextInt(5);
                stars[i].direction = 1;
            } else {
                stars[i].starbitmap = BitmapFactory.decodeResource(
                        getResources(), R.drawable.star1);
                stars[i].y = screenH;
                stars[i].x = random.nextInt(screenW);
                stars[i].speed = random.nextInt(5);
                stars[i].direction = -1;
            }
        }

        bitmapA = BitmapFactory.decodeResource(getResources(), R.drawable.aaa);
        aW = bitmapA.getWidth();
        aH = bitmapA.getHeight();
        aArray = new Point[200];

        thanksPaint = new Paint();
        thanksPaint.setTextSize(textSize1);
        thanksPaint.setColor(Color.BLACK);

        spellingPaint = new Paint();
        spellingPaint.setTextSize(textSize1 + 10);
        spellingPaint.setColor(Color.WHITE);

        spellingPaintShadow = new Paint();
        spellingPaintShadow.setTextSize(textSize1 + 10);
        spellingPaintShadow.setColor(Color.BLACK);

        maxPoints = 175;

    }

    public void incrementCurrColor() {
        // steps color up or down based on current hex code
        if (currColor.equals("#0000FF"))
            cycle = 1;
        else if (currColor.equals("#00FFFF"))
            cycle = 2;
        else if (currColor.equals("#00FF00"))
            cycle = 3;
        else if (currColor.equals("#FFFF00"))
            cycle = 4;
        else if (currColor.equals("#FF0000"))
            cycle = 5;
        else if (currColor.equals("#FF00FF"))
            cycle = 6;

        oldColor = currColor.toCharArray();

        switch (cycle) {
            case 1:
                i = 4;
                stepColorUp(oldColor[i]);
                if (stepNextColorDigit) {
                    i--;
                    stepColorUp(oldColor[i]);
                }
                currColor = new String(oldColor);
                break;
            case 2:
                i = 6;
                stepColorDown(oldColor[i]);
                if (stepNextColorDigit) {
                    i--;
                    stepColorDown(oldColor[i]);
                }
                currColor = new String(oldColor);
                break;
            case 3:
                i = 2;
                stepColorUp(oldColor[i]);
                if (stepNextColorDigit) {
                    i--;
                    stepColorUp(oldColor[i]);
                }
                currColor = new String(oldColor);
                break;
            case 4:
                i = 4;
                stepColorDown(oldColor[i]);
                if (stepNextColorDigit) {
                    i--;
                    stepColorDown(oldColor[i]);
                }
                currColor = new String(oldColor);
                break;
            case 5:
                i = 6;
                stepColorUp(oldColor[i]);
                if (stepNextColorDigit) {
                    i--;
                    stepColorUp(oldColor[i]);
                }
                currColor = new String(oldColor);
                break;
            case 6:
                i = 2;
                stepColorDown(oldColor[i]);
                if (stepNextColorDigit) {
                    i--;
                    stepColorDown(oldColor[i]);
                }
                currColor = new String(oldColor);
                break;
        }
    }

    public void stepColorUp(char c) {
        stepNextColorDigit = false;
        if (c == '0')
            oldColor[i] = '1';
        else if (c == '1')
            oldColor[i] = '2';
        else if (c == '2')
            oldColor[i] = '3';
        else if (c == '3')
            oldColor[i] = '4';
        else if (c == '4')
            oldColor[i] = '5';
        else if (c == '5')
            oldColor[i] = '6';
        else if (c == '6')
            oldColor[i] = '7';
        else if (c == '7')
            oldColor[i] = '8';
        else if (c == '8')
            oldColor[i] = '9';
        else if (c == '9')
            oldColor[i] = 'A';
        else if (c == 'A')
            oldColor[i] = 'B';
        else if (c == 'B')
            oldColor[i] = 'C';
        else if (c == 'C')
            oldColor[i] = 'D';
        else if (c == 'D')
            oldColor[i] = 'E';
        else if (c == 'E')
            oldColor[i] = 'F';
        else if (c == 'F') {
            oldColor[i] = '0';
            stepNextColorDigit = true;
        }
    }

    public void stepColorDown(char c) {
        stepNextColorDigit = false;
        if (c == '0') {
            oldColor[i] = 'F';
            stepNextColorDigit = true;
        } else if (c == '1')
            oldColor[i] = '0';
        else if (c == '2')
            oldColor[i] = '1';
        else if (c == '3')
            oldColor[i] = '2';
        else if (c == '4')
            oldColor[i] = '3';
        else if (c == '5')
            oldColor[i] = '4';
        else if (c == '6')
            oldColor[i] = '5';
        else if (c == '7')
            oldColor[i] = '6';
        else if (c == '8')
            oldColor[i] = '7';
        else if (c == '9')
            oldColor[i] = '8';
        else if (c == 'A')
            oldColor[i] = '9';
        else if (c == 'B')
            oldColor[i] = 'A';
        else if (c == 'C')
            oldColor[i] = 'B';
        else if (c == 'D')
            oldColor[i] = 'C';
        else if (c == 'E')
            oldColor[i] = 'D';
        else if (c == 'F') {
            oldColor[i] = 'E';
        }
    }

    public void setScreenVarsSizes() {
        if (screenH >= 1100 && screenW >= 750) {
            textSize1 = 70;
            starCount = 60;
        } else if (screenH >= 1000 && screenW >= 600) {
            textSize1 = 50;
            starCount = 50;
        } else if (screenH >= 800 && screenW >= 400) {
            textSize1 = 30;
            starCount = 40;
        } else if (screenH >= 400 && screenW >= 300) {
            textSize1 = 20;
            starCount = 30;
        } else if (screenH >= 200 && screenW >= 200) {
            textSize1 = 15;
            starCount = 20;
        } else {
            textSize1 = 10;
            starCount = 15;
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (winThread == null
                || winThread.getState() == Thread.State.TERMINATED) {
            winThread = new WinThread(holder, this);
        }

        // only start if its a new thread, an older thread may still be started
        if (winThread.getState() == Thread.State.NEW)
            winThread.start();

        winThread.setRunning(true);
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {

    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        retry = true;
        winThread.setRunning(false);
        while (retry) {
            try {
                winThread.join();
                retry = false;
            } catch (InterruptedException e) {
                // we will try it again and again
            }
        }
    }
}
