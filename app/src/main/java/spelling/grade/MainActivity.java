package spelling.grade;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements ImageFragment.OnFragmentInteractionListener, SpellCheckUIFragment.OnFragmentInteractionListener {

    MySoundPool mySoundPool;
    AudioManager audioManager;
    ImageFragment imageFragment;
    SpellCheckUIFragment spellCheckUIFragment;
    Toast toast;
    SharedPreferences sharedPreferences;
    ImageData imageData = ImageData.getInstance();

    final String PREFS = "prefs";
    final String CURRENT_IMAGE_NUMBER = "currentImageNumber";
    final String CURRENT_IMAGE_GRADE = "currentImageGrade";
    final String CURRENT_RATING = "currentRating";
    final String CURRENT_CHECK_OR_X = "currentCheckOrX";
    final String CURRENT_SPELLING_TEXT = "currentSpellingText";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = getSharedPreferences(
                PREFS, Context.MODE_PRIVATE);

        if (sharedPreferences.contains(CURRENT_IMAGE_NUMBER)) {
            imageData.currentImageNumber = sharedPreferences.getInt(CURRENT_IMAGE_NUMBER, -1);
        }

        if (sharedPreferences.contains(CURRENT_IMAGE_GRADE)) {
            imageData.currentImageGrade = sharedPreferences.getInt(CURRENT_IMAGE_GRADE, -1);
        }

        if (sharedPreferences.contains(CURRENT_RATING)) {
            imageData.currentRating = sharedPreferences.getFloat(CURRENT_RATING, 0);
        }

        if (sharedPreferences.contains(CURRENT_CHECK_OR_X)) {
            imageData.isCheckShown = sharedPreferences.getBoolean(CURRENT_CHECK_OR_X, true);
        }

        if (sharedPreferences.contains(CURRENT_SPELLING_TEXT)) {
            imageData.currentSpellingText = sharedPreferences.getString(CURRENT_SPELLING_TEXT, "");
        }

        mySoundPool = new MySoundPool(this);

        imageFragment = (ImageFragment) getSupportFragmentManager()
                .findFragmentById(R.id.imagefragment);
        imageFragment.myImageView.setNewImage(imageData.currentImageGrade, imageData.currentImageNumber);

        spellCheckUIFragment = (SpellCheckUIFragment) getSupportFragmentManager()
                .findFragmentById(R.id.spellcheckuifragment);
        spellCheckUIFragment.gradeButtons.init();

    }

    @Override
    public void showToast(String text) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(this, text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onGradeButtonClick(int grade, int picture) {
        imageFragment.setNewImage(grade, picture);
    }

    @Override
    public void playCheckSound() {
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        float streamVolumeCurrent = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float streamVolumeMax = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float volume = streamVolumeCurrent / streamVolumeMax;
        mySoundPool.playSound(1, volume);
    }

    @Override
    public void playXSound() {
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        float streamVolumeCurrent = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float streamVolumeMax = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float volume = streamVolumeCurrent / streamVolumeMax;
        mySoundPool.playSound(2, volume);
    }

    @Override
    public void hideSoftKeyboard() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(
                    getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (NullPointerException npe) {
            //in case the keyboard is shut it will throw an exception
        }
    }

    @Override
    public void startWinActivity() {
        //play win cheers
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        float streamVolumeCurrent = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float streamVolumeMax = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float volume = streamVolumeCurrent / streamVolumeMax;
        mySoundPool.playSound(3, volume);

        imageFragment.setNewImage(-1, -1);
        Intent intent = new Intent(this, WinScreenActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(CURRENT_IMAGE_NUMBER, imageData.currentImageNumber);
        editor.putInt(CURRENT_IMAGE_GRADE, imageData.currentImageGrade);
        editor.putFloat(CURRENT_RATING, imageData.currentRating);
        editor.putBoolean(CURRENT_CHECK_OR_X, imageData.isCheckShown);
        editor.putString(CURRENT_SPELLING_TEXT, spellCheckUIFragment.gradeButtons.spellingTextView.getText().toString());
        editor.apply();
    }
}