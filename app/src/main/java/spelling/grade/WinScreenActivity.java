package spelling.grade;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class WinScreenActivity extends AppCompatActivity {

    WinPanel winPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        winPanel = new WinPanel(this);
        setContentView(winPanel);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
